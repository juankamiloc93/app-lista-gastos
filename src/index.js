import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import WebFont from 'webfontloader';
import Contenedor from './elementos/Contenedor'
import {BrowserRouter, Route, Routes } from 'react-router-dom'
import InicioSesion from './componetes/InicioSesion';
import RegistroUsuarios from './componetes/RegistrousUsuarios';
import GastosPorCategoria from './componetes/GastosPorCategoria';
import ListaDeGastos from './componetes/ListaDeGastos';
import EditarGasto from './componetes/EditarGasto';
import {Helmet} from "react-helmet";
import favicon from "./imagenes/logo.png";
import Fondo from "./elementos/Fondo";
import {AuthProvider} from './contextos/AuthContext';
import RutaProtegida from './componetes/RutaPrivada';

WebFont.load({
  google: {
    families: ['Work Sans:400,500,700', 'sans-serif']
  }
});

const Index = () => {
  return(
    <>
      <Helmet>
        <link rel="shortcut icon" href={favicon} type="image/x-ico"></link>
      </Helmet>
      
      <AuthProvider>        
        <BrowserRouter>
          <Contenedor>
            <Routes>
              <Route path="/iniciar-sesion" element={<InicioSesion/>}></Route>
              <Route path="/crear-cuenta" element={<RegistroUsuarios/>}></Route>
              
              <Route path="/categorias" elemnet={
                <RutaProtegida >
                  <GastosPorCategoria/>
                </RutaProtegida>
              }/>              

              <Route path="/lista" element={
                <RutaProtegida>
                  <ListaDeGastos />
                </RutaProtegida>
              }/>         

              <Route path="/editar/:id" element={
                <RutaProtegida>
                  <EditarGasto />
                </RutaProtegida>
              }/>              

              <Route path="/" element={
                <RutaProtegida>
                  <App />
                </RutaProtegida>   
              } />                     
              
            </Routes>        
          </Contenedor>
        </BrowserRouter>
        <Fondo/>
      </AuthProvider>
    </>
    )
}

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(<Index/>);

