import React, {useState} from "react";
import { Helmet } from "react-helmet";
import { Header, Titulo, ContenedorHeader } from '../elementos/Header';
import  Boton from '../elementos/Boton';
import {Formulario, Input, ContenedorBoton } from '../elementos/ElementosDeFormulario'
import { ReactComponent as SvgLogin} from "../imagenes/registro.svg";
import styled from "styled-components";
import { auth } from "../firebase/firebaseConfig";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { useNavigate } from 'react-router-dom';
import Alerta from "../elementos/Alerta";

const Svg = styled(SvgLogin)`
    width: 100%;
    max-height: 6.25rem;
    margin-bottom: 1.25rem;
`;

const RegistroUsuarios = () => {
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [estadoAlerta, setEstadoAlerta] = useState(false);
    const [alerta, setAlerta] = useState({});

    const handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        switch(name){
            case 'email':
                setEmail(value);
                break;
            case 'password':
                setPassword(value);
                break;
            case 'password2':
                setPassword2(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setEstadoAlerta(false);
        setAlerta({});

        const expresionRegular = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-.]+\.[a-zA-Z0-9.]+/;

        if(!expresionRegular.test(email)){
            let mensaje = 'Por favor ingrese un correo válido';
            setAlerta({tipo: 'error', mensaje});
            setEstadoAlerta(true);
            return;
        }

        if(email === ''){
            let mensaje = 'Por favor ingrese el correo electrónico';
            setAlerta({tipo: 'error', mensaje});
            setEstadoAlerta(true);
            return;
        }

        if(password === ''){
            let mensaje = 'Por favor ingrese la contraseña';
            setAlerta({tipo: 'error', mensaje});
            setEstadoAlerta(true);
            return;
        }

        if(password2 === ''){
            let mensaje = 'Por favor ingrese el repite contraseña';
            setAlerta({tipo: 'error', mensaje});
            setEstadoAlerta(true);
            return;
        }

        if(password != password2){
            let mensaje = 'Las contraseñas no coiciden';
            setAlerta({tipo: 'error', mensaje});
            setEstadoAlerta(true);
            return;
        }

        try {
            await createUserWithEmailAndPassword(auth, email, password);
            navigate('/');        
        }catch(error){
            let mensaje;
            switch (error.code) {
                case 'auth/invalid-password':
                    mensaje = 'La contraseña debe ser de al menos 6 carácteres';
                    break;
                case 'auth/email-already-in-use':
                    mensaje = 'Ya existe una cuenta con el correo electrónico proporsionado';
                    break;
                case 'auth/invalid-email':
                    mensaje = 'El correo electrónico suministrado no es válido';
                default:
                    mensaje = 'Error al intentar crea el usuario';
                    break;
            };
            setAlerta({tipo: 'error', mensaje});
            setEstadoAlerta(true);
        }
    }

    return (
        <>
            <Helmet>
                <title>Crear cuenta</title>
            </Helmet>

            <Header>
                <ContenedorHeader>
                    <Titulo>Crear cuenta</Titulo>
                    <div>
                        <Boton to="/iniciar-sesion">Iniciar sesión</Boton>
                    </div>
                </ContenedorHeader>
            </Header>

            <Formulario onSubmit={handleSubmit}>
                <Svg/>
                <Input
                    type="email"
                    name="email"
                    placeholder="Correo electrónico"
                    value={email}
                    onChange={handleChange}
                />
                <Input
                    type="password"
                    name="password"
                    placeholder="Contraseña"
                    value={password}
                    onChange={handleChange}
                />
                <Input
                    type="password"
                    name="password2"
                    placeholder="Repetir la contraseña"
                    value={password2}
                    onChange={handleChange}
                />
                <ContenedorBoton>
                    <Boton as="button" primario type="submit">Crear cuenta</Boton>
                </ContenedorBoton>                
            </Formulario>
            <Alerta
                tipo={alerta.tipo}
                mensaje={alerta.mensaje}
                estadoAlerta={estadoAlerta}
                setEstadoAlerta={setEstadoAlerta}
            />
        </>
    );
}

export default RegistroUsuarios;