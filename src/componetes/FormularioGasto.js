import React, { useState } from "react";
import { ContenedorFiltros, Formulario, Input, InputGrande, ContenedorBoton } from '../elementos/ElementosDeFormulario';
import Boton from '../elementos/Boton'
import {ReactComponent as IconPlus} from '../imagenes/plus.svg';

const FormularioGastos = () => {

    const [inputDescripcion, setInputDescripcion] = useState('');
    const [inputValor, setInputValor] = useState(0);

    const handleChanhe = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        switch(name){
            case 'descripcion':
                setInputDescripcion(value);
                break;
            case 'valor':
                setInputValor(value.replace(/[^0-9.]/g, ''));
                break;
            default:
                break;
        }
    }

    return (
        <>
            <Formulario>
                <ContenedorFiltros>
                    <p>Select</p>
                    <p>Date picker</p>
                </ContenedorFiltros>
                <div>
                    <Input
                        type="text"
                        name="descripcion"
                        id="descripcion"
                        placeholder="Descripción"
                        value={inputDescripcion}
                        onChange={handleChanhe}
                    />
                    <InputGrande
                        type="text"
                        name="valor"
                        id="valor"
                        placeholder="$0.00"
                        value={inputValor}
                        onChange={handleChanhe}
                    />
                    <ContenedorBoton>
                        <Boton as="button" primario conIcono type="submit">
                            Agregar Gasto <IconPlus/>
                        </Boton>
                    </ContenedorBoton>
                </div>
            </Formulario>
        </>
    );
};

export default FormularioGastos;